Gem::Specification.new do |s|
  s.name        = 'hello'
  s.version     = '0.0.0'
  s.date        = '2020-09-22'
  s.summary     = "Hello!"
  s.description = "A simple hello world gem"
  s.authors     = ["Atlassian"]
  s.email       = 'bitbucketci-team@atlassian.com'
  s.files       = ["lib/hello.rb"]
  s.license     = 'MIT'
end
